package animal

import (
	"fmt"
	"math"
	"strconv"
)

func darken(hex string, amount int) string {
	rgb := hex2Rgb(hex)
	for i, x := range rgb {
		rgb[i] = clamp(x-amount, 0, 255)
	}
	return rgb2Hex(rgb)
}

func lighten(hex string, amount int) string {
	return darken(hex, -amount)
}

func hex2Rgb(hex string) []int {
	if len(hex) == 4 {
		hex = fmt.Sprintf("#%s%s%s%s%s%s", string(hex[1]), string(hex[1]), string(hex[2]), string(hex[2]), string(hex[3]), string(hex[3]))
	}
	pairs := []string{hex[1:3], hex[3:5], hex[5:]}
	rgb := make([]int, len(pairs))
	for i, pair := range pairs {
		value, _ := strconv.ParseInt(pair, 16, 0)
		rgb[i] = int(value)
	}
	return rgb
}

func rgb2Hex(rgb []int) string {
	hex := "#"
	for _, x := range rgb {
		hex += fmt.Sprintf("%02x", x)
	}
	return hex
}

func clamp(value int, min int, max int) int {
	return int(math.Min(float64(max), math.Max(float64(min), float64(value))))
}
