package animal

import (
	"fmt"
	"strings"
)

func createSvg(size int, childen []string) string {
	return `<svg
xmlns="http://www.w3.org/2000/svg"
version="1.1"
width="` + fmt.Sprintf("%d", size) + `"
height="` + fmt.Sprintf("%d", size) + `"
viewBox="0 0 500 500"
>
` + strings.Join(childen, "") + `
</svg>`
}

func createBackground(round bool, color string) string {
	rx := 0
	if round {
		rx = 250
	}
	return `<rect
width="500"
height="500"
rx="` + fmt.Sprintf("%d", rx) + `"
fill="` + color + `"
/>`
}

func createBlackout(round bool) string {
	d := "M250,0a250,250 0 1,1 0,500"
	if !round {
		d = "M250,0L500,0L500,500L250,500"
	}
	return `<path
d="` + d + `"
fill="#15212a"
fill-opacity="0.08"
/>`
}
