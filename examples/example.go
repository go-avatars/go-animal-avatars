package main

import (
	"log"
	"os"

	"src.techknowlogick.com/animal"
)

func main() {
	a := animal.New("animal avatar", 480)
	err := os.WriteFile("animal.svg", []byte(a), 0644)
	if err != nil {
		log.Fatalf("Failed to write to file: %v", err)
	}
}
