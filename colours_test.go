package animal

import (
	"fmt"
	"testing"
)

func TestDarken(t *testing.T) {
	testCases := []struct {
		hex     string
		amount  int
		wantHex string
	}{
		{"#000", 20, "#000000"},
		{"#fff", 20, "#ebebeb"},
		{"#f1f1f1", 20, "#dddddd"},
		{"#fe3", 20, "#ebda1f"},
		{"#ffee33", 20, "#ebda1f"},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%s_%d", tc.hex, tc.amount), func(t *testing.T) {
			got := darken(tc.hex, tc.amount)
			if got != tc.wantHex {
				t.Errorf("expected %s, got %s", tc.wantHex, got)
			}
		})
	}
}

func TestLighten(t *testing.T) {
	testCases := []struct {
		hex     string
		amount  int
		wantHex string
	}{
		{"#000000", 20, "#141414"},
		{"#ebebeb", 20, "#ffffff"},
		{"#dddddd", 20, "#f1f1f1"},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%s_%d", tc.hex, tc.amount), func(t *testing.T) {
			got := lighten(tc.hex, tc.amount)
			if got != tc.wantHex {
				t.Errorf("expected %s, got %s", tc.wantHex, got)
			}
		})
	}
}
