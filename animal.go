package animal

import (
	"hash/fnv"
	"math/rand"
	"strings"
)

func New(seed string, size int) string {
	return NewWithOptions(seed, size, avatarColours, backgroundColours, true, true)
}

func NewWithOptions(seed string, size int, aColours, bColours []string, blackout, round bool) string {
	// Create a local instance of the RNG
	var localRand *rand.Rand
	s := rand.NewSource(stringToSeed(seed))
	localRand = rand.New(s)

	avatarColour := pick(aColours, localRand)

	pattern := ""
	if localRand.Intn(2) == 0 {
		pattern = pick(getPatterns(avatarColour), localRand)
	}
	hair := ""
	if localRand.Intn(2) == 0 {
		hair = pick(getHairs(avatarColour), localRand)
	}

	avatar := []string{
		pick(getFaces(avatarColour), localRand),
		pattern,
		pick(getEars(avatarColour), localRand),
		hair,
		pick(getMuzzles(avatarColour), localRand),
		pick(getEyes(avatarColour), localRand),
		pick(getBrows(avatarColour), localRand),
	}

	if blackout {
		avatar = append(avatar, createBlackout(round))
	}
	return createSvg(size, []string{
		createBackground(round, pick(bColours, localRand)),
		strings.Join(avatar, ""),
	})
}

func pick(list []string, random *rand.Rand) string {
	index := random.Intn(len(list))
	return list[index]
}

func stringToSeed(s string) int64 {
	h := fnv.New64a()
	h.Write([]byte(s))
	return int64(h.Sum64())
}
